#### Personal spec files

Some spec files. Mostly for tools or programs of which I have not found ready-made RPM packages. 

- Sipcalc - A widely distributed IPv4/6 subnet calculator
- Tinyproxy - A small, efficient HTTP/SSL proxy daemon
