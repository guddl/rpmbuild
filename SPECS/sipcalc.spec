Name:           sipcalc
Version:        1.1.6
Release:        1%{?dist}
Summary:        Sipcalc is an advanced console based ip subnet calculator. 

License:        BSD
URL:            http://www.routemeister.net/projects/sipcalc/ 
Source0:        http://www.routemeister.net/projects/sipcalc/%{name}-%{version}.tar.gz

#Requires:       
BuildRequires:  gcc
BuildRequires:  gettext
BuildRequires:  make

%description
Sipcalc is an advanced console based ip subnet calculator.


%prep
%autosetup


%build
%configure
%make_build


%install
rm -rf $RPM_BUILD_ROOT
%make_install

%check
make check


%files
%doc AUTHORS README COPYING TODO NEWS
%{_bindir}/%{name}
%{_mandir}/man1/%{name}.1.*


%changelog
* Sat Sep 24 2022 Andreas Gutowski <guddl@guddl.de> 1.1.6
- Initial Package for Almalinux 9
- 
